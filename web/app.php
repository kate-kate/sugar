<?php
require_once(__DIR__ . '/../vendor/autoload.php');
$kernel = new \Component\Kernel();
$kernel->run(\Component\Kernel::MODE_WEB, require_once(__DIR__.'/../config/config.php'));
