<?php
return [
    'db' => [
        'dsn' => 'mysql:host=localhost;dbname=sugar;charset=utf8',
        'user' => 'root',
        'pass' => 'root'
    ],
    'notifier' => [
        'from' => 'admin@sugar.com'
    ],
    'templatesPath' => __DIR__ . '/../templates'
];