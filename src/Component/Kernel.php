<?php
/**
 * Created by PhpStorm.
 * User: mitalcoi
 * Date: 23.07.15
 * Time: 9:40
 */

namespace Component;

use Pimple\Container;
use Services\NotifierService;
use Symfony\Component\HttpFoundation\Session\Session;

class Kernel
{
    private $isInited = false;
    const MODE_WEB = 'web';
    const MODE_CONSOLE = 'console';

    public function run($mode, array $config)
    {
        if ($this->isInited === true) {
            throw new \Exception("Kernel should be called once");
        }
        $container = $this->getContainer($config);
        if ($mode === self::MODE_WEB) {
            $controller = new \Controller\WebController($container, $config['templatesPath']);
            $controller->handle();
        } elseif ($mode === self::MODE_CONSOLE) {
            $controller = new \Controller\ConsoleController($container, $config['templatesPath']);
            $controller->handle();
        } else {
            throw new \Exception("Unsupported mode");
        }
        $this->isInited = true;
    }

    public function getContainer($config)
    {

        $container = new Container();
        $container['pdo'] = function () use ($config) {
            return new \PDO($config['db']['dsn'], $config['db']['user'], $config['db']['pass']);
        };
        $container['mailer'] = function () use ($config) {
        };
        $container['session'] = function () {
            $session = new Session();
            $session->start();
            return $session;
        };
        $container['notifier'] = function () use ($config) {
            $notifier = new NotifierService($config['notifier']['from'], $config['templatesPath']);
            return $notifier;
        };
        return $container;
    }
} 