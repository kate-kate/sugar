<?php

namespace Component;


use Pimple\Container;

abstract class AbstractController
{
    /**
     * @var Container
     */
    protected $container;
    protected $templatePath;

    /**
     * @param Container $container
     */
    public function __construct(Container $container, $templatePath)
    {
        $this->container = $container;
        $this->templatePath = $templatePath;
    }

    /**
     * @return mixed
     */
    abstract public function handle();
} 