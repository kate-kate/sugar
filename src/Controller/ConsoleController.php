<?php
namespace Controller;

use Component\AbstractController;
use Model\Message;
use Services\NotifierService;

class ConsoleController extends AbstractController
{

    public function handle()
    {
        $this->processMessages();
    }

    protected function processMessages()
    {
        $message = new Message();
        $message->setPdo($this->container['pdo']);
        $messages = $message->getLastMessages(10);
        /** @var NotifierService $notifier */
        $notifier = $this->container['notifier'];
        foreach ($messages as $message) {
            $message->setPdo($this->container['pdo']);
            try {
                $notifier->processMessage($message);
                $message->updateSentStatus();
                echo "send message to {$message->email}\n";
            } catch (\Swift_SwiftException $e) {
                echo "message to {$message->email} is not sent, reason - {$e->getMessage()}";
            }
        }
    }
} 