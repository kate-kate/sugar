<?php
namespace Controller;

use Component\AbstractController;
use Model\Message;
use Pimple\Container;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Gregwar\Captcha\CaptchaBuilder;
use Symfony\Component\HttpFoundation\Response;

class WebController extends AbstractController
{
    /**
     * @var Request
     */
    protected $request;

    public function __construct(Container $container, $templatePath)
    {
        parent::__construct($container, $templatePath);
        $this->request = \Symfony\Component\HttpFoundation\Request::createFromGlobals();
    }

    /** @inheritdoc */
    public function handle()
    {
        if ($this->request->getMethod() === 'GET') {
            $this->processDefault();
        } elseif ($this->request->getMethod() === 'POST') {
            if ($this->request->query->get('action') === 'validate') {
                $this->processValidate();
            } elseif ($this->request->query->get('action') === 'captcha') {
                $this->processCaptcha();
            }
        }
    }

    protected function processDefault()
    {
        $response = new Response(file_get_contents($this->templatePath . '/home.html'));
        $response->send();

    }

    protected function processValidate()
    {
        $message = new Message();
        $message->setPdo($this->container['pdo']);
        $message->setData($this->request->get('Message', []));
        $validateErrors = $message->getErrors();
        $valid = !(bool)$validateErrors;
        $captcha = '';
        if ($valid) {
            $builder = new CaptchaBuilder;
            $builder->build();
            $captcha = $builder->inline();
            $this->container['session']->set('phrase', $builder->getPhrase());
        }
        $res = ['valid' => $valid, 'errors' => $validateErrors, 'captcha' => $captcha];
        $response = new JsonResponse($res);
        $response->send();
    }

    protected function processCaptcha()
    {
        $userC = $this->request->get('captcha');
        $res = ['text' => 'Неверно введена капча', 'code' => 1];
        if ($userC === $this->container['session']->get('phrase')) {
            $message = new Message();
            $message->setPdo($this->container['pdo']);
            $message->setData($this->request->get('Message', []));
            if (!$message->getErrors() && $message->save()) {
                $res = ['text' => 'Успешно сохранено', 'code' => 2];
            } else {
                $res = ['text' => 'Внутренняя ошибка. Пожалуйста, обратитесь к администартору', 'code' => 3];
            }
        }
        $response = new JsonResponse($res);
        $response->send();
    }
} 