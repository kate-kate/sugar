<?php
namespace Services;

use Model\Message;

class NotifierService
{
    protected $from;
    protected $templatesPath;

    public function __construct($from, $templatesPath)
    {
        $this->from = $from;
        $this->templatesPath = $templatesPath;
    }

    public function processMessage(Message $message)
    {
        list ($template, $theme) = $this->detectTemplate($message);
        $mailBody = str_replace(
            ['{name}', '{domain}', '{message}'],
            [$message->name, $this->getDomain($message->email), $message->message],
            file_get_contents($template)
        );
        $this->sendMail($message->email, $theme, $mailBody);
    }

    protected function detectTemplate(Message $message)
    {
        if (!in_array($this->getDomain($message->email), ['mail.ru', 'rambler.ru', 'yandex.ru'])) {
            return [$this->templatesPath . '/mails/template1.html', 'К сожалению, мы не можем вам помочь'];
        } else {
            return [$this->templatesPath . '/mails/template2.html', 'Всё отлично'];
        }
    }

    protected function getDomain($email)
    {
        $arr = explode('@', $email);
        return $arr[1];
    }

    /**
     * @param $to
     * @param $subject
     * @param $body
     * @return int
     */
    protected function sendMail($to, $subject, $body)
    {
        $message = new \Swift_Message();
        $message
            ->setFrom($this->from)
            ->setSubject($subject)
            ->setTo([
                $to
            ])
            ->setBody($body);
        $this->getMailer()->send($message);
    }

    protected $mailer;

    /**
     * @return \Swift_Mailer
     */
    protected function getMailer()
    {
        if (!$this->mailer) {
            $transport = \Swift_MailTransport::newInstance();
            $this->mailer = \Swift_Mailer::newInstance($transport);
        }
        return $this->mailer;
    }
}