<?php
namespace Model;

use Symfony\Component\Validator\Constraints\DateTime;

class Message
{
    public $id;
    public $name;
    public $message;
    public $email;
    protected $pdo;
    const DATE_FORMAT = 'Y-m-d H:i:s';

    public function setPdo(\PDO $pdo)
    {
        $this->pdo = $pdo;
    }

    public function setData(array $data)
    {
        foreach ($this->attributes() as $attr) {
            if (isset($data[$attr])) {
                $this->{$attr} = $data[$attr];
            }
        }

    }

    public function attributes()
    {
        return ['name', 'message', 'email'];
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function save()
    {
        if ($this->pdo) {
            $stm = $this->pdo->prepare("INSERT INTO message (name, message, email, date_created) values (?, ?, ?, ?)");
            return $stm->execute([
                $this->name,
                $this->message,
                $this->email,
                (new \DateTime())->format(self::DATE_FORMAT)
            ]);
        } else {
            throw new \Exception("PDO not configured");
        }
    }

    public function updateSentStatus()
    {
        if ($this->pdo) {
            $stm = $this->pdo->prepare("UPDATE message SET sent=1 WHERE id=?");
            return $stm->execute([$this->id]);
        } else {
            throw new \Exception("PDO not configured");
        }
    }

    public function getErrors()
    {
        $errors = [];
        if (!$this->email) {
            $errors['email'] = 'Укажите email';
        } elseif (filter_var($this->email, FILTER_VALIDATE_EMAIL) === false) {
            $errors['email'] = 'Неправильный email';
        }

        if (!$this->name) {
            $errors['name'] = 'Укажите Ваше имя';
        }

        if (!$this->message) {
            $errors['message'] = 'Заполните текст сообщения';
        }
        return $errors;
    }

    /**
     * @param int $minutes
     * @return Message[]
     * @throws \Exception
     */
    public function getLastMessages($minutes = 10)
    {
        if ($this->pdo) {
            $start_date = (new \DateTime())->sub(new \DateInterval("PT{$minutes}M"))->format(self::DATE_FORMAT);
            $end_date = (new \DateTime())->format(self::DATE_FORMAT);
            $stm = $this->pdo->prepare("SELECT id, email, name, message FROM message WHERE sent=0 AND date_created BETWEEN :start_date AND :end_date");
            $stm->bindParam(':start_date', $start_date, \PDO::PARAM_STR);
            $stm->bindParam(':end_date', $end_date, \PDO::PARAM_STR);
            $stm->execute();
            return $stm->fetchAll(\PDO::FETCH_CLASS, get_called_class());
        } else {
            throw new \Exception("PDO not configured");
        }
    }
} 